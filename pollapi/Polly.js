const axios = require('axios');
export default class Polly {
    constructor() {
        console.log('POLLY!');
    }

    static get serverEndpoint() {
        return SERV_URL + ":" + SERV_PORT;
    }

    SaveAnswer() {

    }

    SetEmployeeQuestion(device_id, question_id) {
        return new Promise((resolve, reject) => {
            let url = this.BuildGetLink(['question', question_id, "device", device_id]);
            axios.put(url)
                .then((response) => {
                    resolve(response.data);
                })
                .catch((error) => {
                    reject({error: error});
                });
        })
    }

    GetAvailableQuestions(handler, app_id) {
        this.GetCurrentQuestions(app_id)
            .then((response) => {
                handler(response);
            })
            .catch((error) => {
                handler({error: "No questions."});
            });
    }

    GetCurrentQuestions(app_id) {
        return new Promise((resolve, reject) => {

            let now = new Date();
            let nowHours = now.getHours() * 60 * 60;
            let nowMinutes = now.getMinutes() * 60;
            let nowSec = now.getSeconds();
            let time = nowHours + nowMinutes + nowSec;
            let url = this.BuildGetLink(['questions', app_id, time]);

            axios.get(url)
                .then((response) => {
                    resolve(response.data);
                })
                .catch((error) => {
                    console.log('Get Questions Error:');
                    console.log(error);
                    reject({error: 501});
                });
        });
    }

    SendAnswer(answer, device_id) {
        return new Promise((resolve, reject) => {
            let now = new Date();
            let nowHours = now.getHours() * 60 * 60;
            let nowMinutes = now.getMinutes() * 60;
            let nowSec = now.getSeconds();
            let time = nowHours + nowMinutes + nowSec;
            let url = this.BuildGetLink(['appid', device_id, 'time', time]);
            axios.post(url, {
                answer: answer
            })
                .then((response) => {
                    resolve(response.data);
                })
                .catch((error) => {
                    reject({error: error})
                });
        });
    }

    BuildGetLink(parts) {
        let url = Polly.serverEndpoint;
        for (let part in parts) {
            url += '/' + parts[part];
        }
        return url;
    }

    SetDeviceId(employee_id, device_id, app_id) {
        return new Promise((resolve, reject) => {
            axios.get(Polly.serverEndpoint + '/employee/' + employee_id + '/' + device_id + '/' + app_id)
                .then((response) => {
                    resolve(response.data);
                })
                .catch((err) => {
                    reject({error: 501});
                })
        });
    }
}

const SERV_URL = 'http://10.2.0.75',
    SERV_PORT = 7777;