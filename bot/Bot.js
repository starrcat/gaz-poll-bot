const Telegraph = require('telegraf');
import Polly from '../pollapi/Polly';
import Watson from "../watson/Watson";

export default class Bot {
    constructor(token) {
        this.BOT_TOKEN = token;
        this.name = 'Арина';
        this.bot = new Telegraph(this.BOT_TOKEN);
        this.bot.start((ctx) => { this.OnConversationStarts(ctx) });
        this.watson = new Watson();

        // Start Polly
        this.polly = new Polly();
        this.polly.GetAvailableQuestions(this.OnQuestionsAvailable.bind(this), 'telegram');

        this.SubscribeAll();
    }

    Start() {
        this.bot.startPolling();
    }

    OnTextMessageReceived(ctx) {
        let answer = this.GetMessageText(ctx);
        console.log("ANSEWR: ", answer);
        let device_id = ctx.chat.id;
        this.polly.SendAnswer(answer, device_id)
            .then((response) => {
                console.log('Answer sent: ');
                console.log(response);
                if (response.length > 0 && response[0].question) {
                    this.AskQuestion(response[0].question, response[0].varAnswers, device_id);
                } else {
                    this.HideKeyboard(ctx);
                }
            })
            .catch((error) => {
                if (error.error.response.status === 500) {
                    this.watson.SendMessage(answer)
                        .then((res) => {
                            ctx.reply(res.answer);
                        })
                        .catch((err) => {
                            console.log(err);
                        });
                }
            });

        // Send text to watson
        // this.watson.SendMessage(text)
        //     .then((response) => {
        //         ctx.reply(response.answer);
        //     })
        //     .catch((err) => {
        //         console.log(err);
        //     });
    }

    HideKeyboard(ctx) {
        ctx.reply("Спасибо большое! Вы ответили на все доступные вопросы! 😘", {
            reply_markup: {
                remove_keyboard: true
            }
        });
    }

    SubscribeAll() {
        // Когда получили Табельный номер
        this.SubscribePhrase(/^\d{4,}$/, this.OnEmployeeIdReceived.bind(this));
        this.SubscribePhrase('key', this.OnKeyReceived.bind(this));
        this.bot.on('text', (ctx) => { this.OnTextMessageReceived(ctx); });
    }

    // Subscribe bot for a specific phrase
    SubscribePhrase(phrase, phrase_handler) {
        this.bot.hears(phrase, (ctx) => {phrase_handler(ctx)})
    }

    // Returns user id from telegram.
    GetTelegramId(ctx) {
        // return ctx.from.id;
        return ctx.chat.id;
    }

    GetMessageText(ctx) {
        return ctx.message.text;
    }

    // When user just started chat with bot.
    OnConversationStarts(ctx) {
        ctx.replyWithMarkdown("Здравствуйте!\nМеня зовут Арина. Я провожу ежегодный опрос сотрудников ГАЗ. Если вы готовы начать опрос, пожалуйста, отправьте мне Ваш *табельный номер* в ответном сообщении.");
    }

    OnEmployeeIdReceived(ctx) {
        let eid = parseInt(ctx.message.text);
        let tlgId = this.GetTelegramId(ctx);
        this.updateDeviceId(eid, tlgId)
            .then((result) => {
                if (result) {
                    ctx.reply('Спасибо! Теперь мы можем начать. 👍');
                    this.polly.GetAvailableQuestions(this.OnQuestionsAvailable.bind(this), 'telegram');
                }
            })
            .catch((error) => {
                ctx.reply('Не могу найти вас в базе данных. Возможно, вы ввели неправильный табельный номер. Попробуйте ещё раз.☺')
            });
    }

    OnKeyReceived(ctx) {
        let keyboard = [['1','2','3']];
        ctx.reply("Вот вам клавиатура:", {
            reply_markup: {
                keyboard: keyboard,
                one_time_keyboard: true
            }
        });
    }

    // When user puts /help command.
    OnHelp() {

    }

    OnQuestionsAvailable(questions) {
        for (let question in questions) {
            let quest = questions[question];
            if (quest.employee) {
                if (quest.employee.deviceId) {
                    let deviceId = quest.employee.deviceId;
                    // Getting question object
                    if (quest.question) {
                        this.AskQuestion(quest.question, quest.varAnswers, deviceId);
                    }
                }
            }
        }
        // Запомним, что нам нужно сделать очередной запрос на новые вопросы
        setTimeout(() => {
            this.polly.GetAvailableQuestions(this.OnQuestionsAvailable.bind(this), 'telegram');
        }, 5 * 1000);
    }

    AskQuestion(question, varAnswers, deviceId) {
        console.log("Asking ", deviceId);

        let keyboard = [];
        let reply_markup = {};

        if (varAnswers !== null) {
            for (let ans in varAnswers) {
                keyboard.push([varAnswers[ans].answer]);
            }
            reply_markup['keyboard'] = keyboard;
            reply_markup['one_time_keyboard'] = true;
        }

        if (keyboard.length === 0) {
            reply_markup['remove_keyboard'] = true;
        }

        let questionText = "*Вопрос №" + question.questionId + "*\n";
        questionText += question.question;

        this.bot.telegram.sendMessage(deviceId, questionText, {
            parse_mode: 'Markdown',
            reply_markup: reply_markup // Отправим клавиатуру.
        })
            .then((response) => {
                if (response.message_id && response.chat && response.chat.id === parseInt(deviceId)) {
                    // Вопрос успешно отправлен, сообщаем это серверу.
                    this.polly.SetEmployeeQuestion(deviceId, question.questionId)
                        .then((response) => {
                            console.log("CURRENT QUESTION UPDATED!");
                        })
                        .catch((error) => {
                            console.log("ERROR ON UPDATING QUESTION!");
                        });
                }
            })
            .catch((error) => {
                console.log("ERROR ON ASKING!");
                console.log(error);
                console.log("---- END OF ERROR ON ASKING! ------");
            });
    }

    updateDeviceId(eid, tid) {
        return new Promise((resolve, reject) => {
            this.polly.SetDeviceId(eid, tid, 'telegram')
                .then((result) => {
                    if (result.employeeId !== undefined && result.deviceId !== undefined) {
                        if (parseInt(result.employeeId) === parseInt(eid) && parseInt(result.deviceId) === parseInt(tid)) {
                            resolve(true);
                        }
                    } else {
                        reject(false);
                    }
                })
                .catch((err) => {
                    reject(false);
                });
        });
    }
}