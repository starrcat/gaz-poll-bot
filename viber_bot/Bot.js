import Polly from '../pollapi/Polly';

const ngrok = require('./getPublishURL');
const ViberBot = require('viber-bot').Bot;
const BotEvents = require('viber-bot').Events;
const TextMessage = require('viber-bot').Message.Text;
const KeyboardMessage = require('viber-bot').Message.Keyboard;
const KeyboardGeneratorModule = require('./KeyboardGenerator.js');
let http = require("http");
const axios = require('axios');
const port = process.env.PORT || 8080;
import Watson from "../watson/Watson";

export default class Bot {
    constructor(token) {
        this.BOT_TOKEN = token;
        this.test = "Testing!";
        this.watson = new Watson();

        this.userProfiles = [{
            id: '1aInq7SelCKE9tERd9nhgA==',
            name: 'Alexander',
            avatar: null,
            country: 'RU',
            language: 'ru',
            apiVersion: 7 }];

        // Creating the bot with access token, name and avatar
        this.bot = new ViberBot({
            authToken: this.BOT_TOKEN,
            name: "Арина",
            avatar: "https://dl-media.viber.com/5/share/2/long/vibes/icon/image/0x0/02c5/b808048be9fcee8126c82acf43064a1b926f776be02f309cae4779755fdc02c5.jpg"});

        // Класс взаимодействующий с базой-данных Димана.
        this.polly = new Polly();

        this.bot.onConversationStarted((userProfile, isSubscribed, context, onFinish) => {
            onFinish(new TextMessage('Здравствуйте!\nМеня зовут Арина. Я провожу ежегодный опрос сотрудников ГАЗ. Если вы готовы начать опрос, пожалуйста, отправьте мне Ваш *табельный номер* в ответном сообщении.'));
        });

        //Регистрация нового табеля
        this.bot.onTextMessage(/^\d{4,}$/, (message, response) => {
            console.log(response.userProfile);
           this.userProfiles.push(response.userProfile);
           this.polly.SetDeviceId(message.text, response.userProfile.id, "viber")
               .then(result => {
                   if (result.employeeId && result.deviceId) {
                       this.bot.sendMessage(response.userProfile, new TextMessage("Спасибо! Теперь мы можем начать."));
                   }
                   else {
                       this.bot.sendMessage(response.userProfile, new TextMessage("Не могу найти вас в базе данных. Возможно, вы ввели неправильный табельный номер. Попробуйте ещё раз.☺"));
                   }
               })
               .catch(error => {
                   this.bot.sendMessage(response.userProfile, new TextMessage("Не могу найти вас в базе данных. Возможно, вы ввели неправильный табельный номер. Попробуйте ещё раз.☺"));
               })
        });

        this.polly.GetAvailableQuestions(this.OnQuestionsAvailable.bind(this), "viber");

        //Обработка любого сообщения
        this.bot.onTextMessage(/.*/, (message,response) => {

            /*this.polly.SendAnswer(message.text, response.userProfile.id)
                .then((res) => {
                    if(res.)
                })
                .catch((err) => {
                    console.log("Error while sending answer " + err);
                });*/

            // TODO: Test text on answer or send to Watson

            // Send text to watson
            /*this.watson.SendMessage(message.text)
                .then((res) => {
                    this.bot.sendMessage(response.userProfile, new TextMessage(res.answer));
                })
                .catch((err) => {
                    console.log(err);
                });*/

        });
    }

    Start() {
        return ngrok.getPublicUrl().then(publicUrl => {
            console.log('Set the new webhook to', publicUrl);
            http.createServer(this.bot.middleware()).listen(port, () => this.bot.setWebhook(publicUrl));
        }).catch(error => {
            console.log('Can not connect to ngrok server. Is it running?');
            console.error(error);
        });
    }

    OnQuestionsAvailable(questions) {
        for (let question in questions) {
            let quest = questions[question];
            if (quest.employee) {
                if (quest.employee.deviceId) {
                    let deviceId = quest.employee.deviceId;
                    // Getting question object
                    if (quest.question) {
                        this.AskQuestion(quest.question, quest.varAnswers, deviceId);
                    }
                }
            }
        }
    }

    AskQuestion(question, varAnswers, deviceId) {
        console.log("Asking ", deviceId);

        let keyboard_buttons = {
            "Type": "keyboard",
            "Revision": 1,
            "Buttons": []
        };

        if (varAnswers !== null) {
            for (let ans in varAnswers) {
                let button = {
                    "BgColor": "#e6f5ff",
                    "ActionType": "reply",
                    "ActionBody": varAnswers[ans].answer,
                    "Text": varAnswers[ans].answer
                };
                keyboard_buttons.Buttons.push(button);
            }
        }
        else {
            //Если это текстовый вопрос
        }

        console.log(keyboard_buttons);
        this.bot.sendMessage(this.GetUserProfileById(deviceId)[0],
            new TextMessage("Вопрос №" + question.questionId + ":\n" + question.question,
            keyboard_buttons))
            .then((response) => {
                    // Вопрос успешно отправлен, сообщаем это серверу.
                /*this.polly.SetEmployeeQuestion(deviceId, question.questionId)
                    .then((response) => {
                        console.log("CURRENT QUESTION UPDATED!");
                    })
                    .catch((error) => {
                        console.log("ERROR ON UPDATING QUESTION!");
                    });*/
            })
            .catch((error) => {
                console.log("ERROR ON ASKING!");
                console.log(error);
                console.log("---- END OF ERROR ON ASKING! ------");
            });
    }

    GetUserProfileById (id) {
        return this.userProfiles.filter(profile => profile.id === id);
    }
}