
const watson = require('watson-developer-cloud');

export default class Watson {
    constructor() {
        this.workspaceId = '6fa03cb8-5d88-43f6-9c3b-e3dd1f44425a';
        this.assistant = new watson.AssistantV1({
            username: '9a93f976-43a8-4d41-ae94-ccd4a14a3359',
            password: 'mgwI5tPuVWl4',
            version: '2018-10-18',
            url: 'https://gateway.watsonplatform.net/assistant/api'
        });
        this.context = {};
    }

    SendMessage(text) {
        return new Promise((resolve, reject) => {
            this.assistant.message({
                workspace_id: this.workspaceId,
                input: {text: text},
                context: this.context
            }, (err, response) => {
                if (err) {
                    console.log('Watson Error: ');
                    console.log(err);
                    reject({error: err});
                } else {
                    if (response.context) {
                        this.context = response.context;
                    }
                    if (response.output) {
                        if (response.output.text) {
                            resolve({answer: this.GenerateAnswer(response.output.text)});
                        }
                    }
                }
            });
        });
    }

    GenerateAnswer(text) {
        let answer = "";
        for (let txt in text) {
            answer += text[txt] + '\n';
        }
        return answer.slice(0, -1);
    }

    ClearContext() {
        this.context = {};
    }

}